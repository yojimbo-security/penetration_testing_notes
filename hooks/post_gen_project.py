import os

environment = "{{ cookiecutter.environment }}"

ad_targets = "{{ cookiecutter.ad_targets }}"
app_targets = "{{ cookiecutter.app_targets }}"

def make_pkl_file(env_type, targets):
    count=0
    mapped=""
    for target in targets:
        mapped+=f"\"{target}\", \"192.168.1.{count}\", "
        count+=1
    pkl_file= f"{env_type}"+""" {
    // replace IP addreses with your data
    // pkl eval -f yaml create_targets.pkl > targets.yaml
    for (_name, _ip in Map("""+mapped.rstrip(', ')+""")){
        [_name]{
            ip = _ip
            hostname=""
            os{
                name=""
                version=""
                arch=""
            }
        }
    }
}
    """
    with open(f"{env_type}/create_targets.pkl", mode="w")as f:
        f.write(pkl_file)

def make_subdirs(path: str):
    """
    path should be app/app_name or ad/vm_name
    """
    subdirectories = ["scans", "credentials", "evidence", "notes", "files", "vulns"]
    for subdir in subdirectories:
        os.makedirs(f"{path}/{subdir}", exist_ok=True)

def create_env(env_type, targets):
    os.makedirs(env_type, exist_ok=True)
    targets=targets.replace(" ", "").split(',')
    make_pkl_file(env_type, targets)
    for target in targets:
        app_path = f"{env_type}/{target}"
        os.makedirs(app_path, exist_ok=True)
        make_subdirs(app_path)

if environment == "Web App":
    create_env("app", app_targets)

elif environment == "Active Directory":
    create_env("ad", ad_targets)

elif environment == "All":
   create_env("ad", ad_targets)
   create_env("app", app_targets)
